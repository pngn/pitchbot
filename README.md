# pitchbot

A discord bot for Japanese pitch accent.

## Running

The bot uses node as a JavaScript runtime. It is tested with node version 16.10.0.

Run the server with the following command. It will listen on port 1080.

```sh
node main.js
```

## Deploying

The following steps are an example for how the bot could be deployed.

Create an ssh config for the host

```sh
cat >> ~/.ssh/config
Host iroiro
    HostName ip.address.of.host

```

Create a git remote on the host

```sh
ssh iroiro
mkdir pitchbot
cd pitchbot/
git init
git checkout -b main
git branch -D master
```

Locally set up a git remote for the deployment location and push the code up

```sh
git remote add prod iroiro:pitchbot
git push prod main
```

Install nodemon on the server

```sh
npm install -g nodemon
nodenv rehash # update PATH for your node installation
cd pitchbot
nodemon
```

Install nginx and configure certbot for Let's Encrypt.

Configure nginx as a proxy with TLS termination

```
sudo vim /etc/nginx/sites-available/default

server {
    # ...

    # Add index.php to the list if you are using PHP
    index index.html index.htm index.nginx-debian.html;
    server_name host.gtld; # managed by Certbot

    location / {
        # serve all content locally on port 1080
        proxy_pass http://127.0.0.1:1080;
    }


    # ...
    listen [::]:443 ssl ipv6only=on; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    # ...
}

sudo systemctl restart nginx
```
